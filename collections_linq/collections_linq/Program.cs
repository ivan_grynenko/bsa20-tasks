﻿using collections_linq.Models;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Timers;
using System.Linq;

namespace collections_linq
{
    class Program
    {
        private static Timer timer = new Timer(1000);
        private static object locker = new object();
        private static string[] menu =
        {
            "Get a number of tasks of a certain user",
            "Get the list of tasks assigned to a particular user",
            "Get tasks completed in the current year for a particular user",
            "Get the ordered list of teams, which members are 10 years and older",
            "Get the list of users alphabetical order with filtered tasks",
            "Get the User structure",
            "Get the Project structure",
            "Run background worker",
            "Exit"
        };

        static async Task Main(string[] args)
        {
            var worker = new DataWorker();

            while (true)
            {
                Console.Clear();
                int menuPoint = 0;
                ConsoleKeyInfo userInput;

                do
                {
                    await DisplayMenu(menuPoint);
                    userInput = Console.ReadKey();

                    if (userInput.Key == ConsoleKey.UpArrow)
                        menuPoint = await DisplayMenu(--menuPoint);

                    if (userInput.Key == ConsoleKey.DownArrow)
                        menuPoint = await DisplayMenu(++menuPoint);

                } while (userInput.Key != ConsoleKey.Enter);

                if (menu[menuPoint] == "Exit")
                {
                    worker.Dispose();
                    Environment.Exit(0);
                }

                if (menu[menuPoint] == "Run background worker")
                {
                    MarkRandomTaskWithDelay(worker, 1000);
                }

                if (menu[menuPoint] == "Get a number of tasks of a certain user")
                {
                    await SetInternalDisplay();

                    Console.WriteLine("Enter Id: ");
                    var id = GetNumInput();

                    try
                    {
                        var result = await worker.GetResponse<Dictionary<string, int>>($"api/tasks/{id}/byuser");

                        if (result != null)
                        {
                            foreach (var item in result)
                                Console.WriteLine($"Project: {item.Key}, number: {item.Value}");
                        }
                        else
                        {
                            Console.WriteLine("Invalid Id");
                        }
                    }
                    catch (Exception e)
                    {
                        await ErrorHandler(e);
                    }
                }

                if (menu[menuPoint] == "Get the list of tasks assigned to a particular user")
                {
                    await SetInternalDisplay();

                    Console.WriteLine("Enter Id: ");
                    var id = GetNumInput();

                    try
                    {
                        var maxLength = 45;
                        var result = await worker.GetMultipleResponse<Tasks>($"api/tasks/{id}/withmaxlength/{maxLength}");

                        if (result != null)
                        {
                            foreach (var item in result)
                                Console.WriteLine($"Task: {item.Name}");
                        }
                        else
                        {
                            Console.WriteLine("Invalid Id or the are no such tasks");
                        }
                    }
                    catch (Exception e)
                    {
                        await ErrorHandler(e);
                    }
                }

                if (menu[menuPoint] == "Get tasks completed in the current year for a particular user")
                {
                    await SetInternalDisplay();

                    Console.WriteLine("Enter Id: ");
                    var id = GetNumInput();

                    try
                    {
                        var result = await worker.GetMultipleResponse<Tasks>($"api/tasks/{id}/currentyear");

                        if (result != null)
                        {
                            foreach (var item in result)
                                Console.WriteLine($"Task: {item.Name}");
                        }
                        else
                        {
                            Console.WriteLine("Invalid Id");
                        }
                    }
                    catch (Exception e)
                    {
                        await ErrorHandler(e);
                    }
                }

                if (menu[menuPoint] == "Get the ordered list of teams, which members are 10 years and older")
                {
                    await SetInternalDisplay();

                    try
                    {
                        var minAge = 10;
                        var result = await worker.GetMultipleResponse<object>($"api/teams/minage/{minAge}");

                        if (result != null)
                        {
                            foreach (var item in (dynamic)result)
                            {
                                Console.WriteLine($"Id: {item.id}, Team: {item.name}, Memebers:");

                                foreach (var member in item.users)
                                    Console.WriteLine($" - {member.firstName} {member.lastName}");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Invalid Id");
                        }
                    }
                    catch (Exception e)
                    {
                        await ErrorHandler(e);
                    }
                }

                if (menu[menuPoint] == "Get the list of users alphabetical order with filtered tasks")
                {
                    await SetInternalDisplay();

                    try
                    {
                        var result = await worker.GetMultipleResponse<UserTasks>("api/users/orderwithtasks");

                        if (result != null)
                        {
                            foreach (var item in result)
                            {
                                Console.WriteLine($"User: {item.User}, Tasks:");

                                foreach (var task in item.Tasks)
                                    Console.WriteLine($" - Task: {task.Name}");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Invalid Id");
                        }
                    }
                    catch (Exception e)
                    {
                        await ErrorHandler(e);
                    }
                }

                if (menu[menuPoint] == "Get the User structure")
                {
                    await SetInternalDisplay();

                    Console.WriteLine("Enter Id: ");
                    var id = GetNumInput();

                    try
                    {
                        var result = await worker.GetResponse<UserInfo>($"api/users/{id}/userinfo");

                        if (result != null)
                        {
                            Console.WriteLine($"User {result.User?.FirstName} {result.User?.LastName}");
                            Console.WriteLine($"Last project: {result.LastProject?.Name}");
                            Console.WriteLine($"Longest task: {result.LongestTask?.Name}");
                            Console.WriteLine($"Number of tasks: {result.OverallNumOfTasks?.ToString()}");
                            Console.WriteLine($"Incomplete tasks: {result.OverallNumOfCanceledTasks?.ToString()}");
                        }
                        else
                        {
                            Console.WriteLine("Invalid Id");
                        }
                    }
                    catch (Exception e)
                    {
                        await ErrorHandler(e);
                    }
                }

                if (menu[menuPoint] == "Get the Project structure")
                {
                    await SetInternalDisplay();

                    try
                    {
                        var result = await worker.GetMultipleResponse<ProjectInfo>("api/projects/withtasks");

                        if (result != null)
                        {
                            foreach (var item in result)
                            {
                                Console.WriteLine($"Project: {item.Project.Id} {item.Project.Name}, " +
                                    $"Longest task: {item.LongestTask.Name}, " +
                                    $"Shortest task: {item.ShortestTask.Name}");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Invalid Id");
                        }
                    }
                    catch (Exception e)
                    {
                        await ErrorHandler(e);
                    }
                }

                Console.WriteLine("\nPress any key ro return");
                Console.ReadKey();
            }
        }

        private async static Task<int> DisplayMenu(int selectedRow)
        {
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("Use UpArrow and DownArrow keys to navigate. Then press Enter. Select \"Exit\" to close the app." + Environment.NewLine);

            if (selectedRow < 0)
                selectedRow = 0;

            if (selectedRow > menu.Length - 1)
                selectedRow = menu.Length - 1;

            await Task.Run(() =>
            {
                lock (locker)
                {
                    for (int i = 0; i < menu.Length; i++)
                    {
                        if (i == selectedRow)
                        {
                            Console.BackgroundColor = ConsoleColor.Yellow;
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.WriteLine(menu[i]);
                            Console.ResetColor();
                        }
                        else
                            Console.WriteLine(menu[i]);
                    }
                }
            });

            return selectedRow;
        }

        private async static Task SetInternalDisplay()
        {
            await Task.Run(() =>
            {
                Console.Clear();
                Console.SetCursorPosition(0, 0);
            });
        }

        private static int GetNumInput()
        {
            var input = Console.ReadLine();
            int result;

            while (!int.TryParse(input, out result))
            {
                Console.WriteLine("It has to be a number. Try again!");
                input = Console.ReadLine();
            }

            return result;
        }

        private async static Task ErrorHandler(Exception e)
        {
            await Task.Run(() =>
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.Message);
                Console.ResetColor();
            });
        }

        private static void MarkRandomTaskWithDelay(DataWorker worker, int delay)
        {
            timer.Elapsed += async (o, e) =>
            {
                var tcs = new TaskCompletionSource<bool>();
                Random random = new Random();

                var result = await worker.GetMultipleResponse<Tasks>("api/tasks");
                var tasks = result.Where(e => e.State != TaskStates.Finished);

                if (tasks.Count() > 0)
                {
                    var task = tasks.ElementAt(random.Next(0, tasks.Count() - 1));
                    task.State = TaskStates.Finished;;
                    var response = await worker.Patch("http://localhost:61631/api/tasks", task);

                    if (response.IsSuccessStatusCode)
                    {
                        var left = Console.CursorLeft;
                        var top = Console.CursorTop;
                        Console.SetCursorPosition(0, 1);
                        Console.Write(new string(' ', 90));
                        Console.SetCursorPosition(10, 1);
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write($"The status of the task `{task.Name}` was updated");
                        Console.ResetColor();
                        Console.SetCursorPosition(left, top);
                        tcs.SetResult(true);
                    }
                    else
                        throw new Exception();
                }

                await Task.Delay(delay);
            };
            timer.AutoReset = true;
            timer.Start();
        }
    }
}
