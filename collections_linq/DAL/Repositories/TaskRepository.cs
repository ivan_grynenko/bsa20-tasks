﻿using DAL.Base;
using DAL.Interfaces;
using DAL.Models;

namespace DAL.Repositories
{
    public class TaskRepository : Repository<Tasks>, ITaskRepository
    {
        public TaskRepository(ApplicationContext context)
            : base(context)
        { }
    }
}
