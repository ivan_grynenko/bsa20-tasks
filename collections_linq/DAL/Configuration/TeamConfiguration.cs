﻿using Bogus;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Linq;

namespace DAL.Configuration
{
    public class TeamConfiguration : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> builder)
        {
            builder.ToTable("Teams");
            builder.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(50)
                .HasColumnName("TeamName");
            builder.HasCheckConstraint("CK_Teams_CreatedAt", "[CreatedAt] <= getdate()");

            var teams = new string[] { "Team A", "Team B", "Team C", "Team D", "Team E" };
            var id = 1;
            var fake = new Faker<Team>()
                .RuleFor(t => t.Id, f => id++)
                .RuleFor(t => t.Name, f => teams[id - 2])
                .RuleFor(t => t.CreatedAt, f => f.Date.Between(new DateTime(2015, 1, 1), DateTime.Now));
            var basicData = Enumerable.Range(1, 5)
                .Select(e => fake.Generate());

            builder.HasData(basicData);
        }
    }
}
