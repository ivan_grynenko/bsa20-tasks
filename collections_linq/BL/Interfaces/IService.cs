﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BL.Interfaces
{
    public interface IService<TEntity>
    {
        Task<IEnumerable<TEntity>> GetAll();
        Task<TEntity> GetById(int id);
        Task Add(IEnumerable<TEntity> entities);
        Task Add(TEntity entity);
        Task Remove(IEnumerable<TEntity> entities);
        Task Remove(TEntity entity);
        Task Update(IEnumerable<TEntity> entities);
        Task Update(TEntity entity);
    }
}
