﻿using BL.Interfaces;
using BL.Models;
using DAL.Base;
using DAL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork unitOFWork;

        public ProjectService(IUnitOfWork unitOFWork)
        {
            this.unitOFWork = unitOFWork;
        }

        public async Task<IEnumerable<Project>> GetAll()
        {
            return await unitOFWork.ProjectRepository.GetAll();
        }

        public async Task<Project> GetById(int id)
        {
            return await unitOFWork.ProjectRepository.GetById(id);
        }

        public async Task Add(IEnumerable<Project> entities)
        {
            await unitOFWork.ProjectRepository.Add(entities);
            await unitOFWork.SaveChanges();
        }

        public async Task Add(Project entity)
        {
            await unitOFWork.ProjectRepository.Add(entity);
            await unitOFWork.SaveChanges();
        }

        public async Task Remove(IEnumerable<Project> entities)
        {
            unitOFWork.ProjectRepository.Remove(entities);
            await unitOFWork.SaveChanges();
        }

        public async Task Remove(Project entity)
        {
            unitOFWork.ProjectRepository.Remove(entity);
            await unitOFWork.SaveChanges();
        }

        public async Task Update(IEnumerable<Project> entities)
        {
            unitOFWork.ProjectRepository.Update(entities);
            await unitOFWork.SaveChanges();
        }

        public async Task Update(Project entity)
        {
            unitOFWork.ProjectRepository.Update(entity);
            await unitOFWork.SaveChanges();
        }

        public async Task<IEnumerable<ProjectInfo>> GetProjectWithTasks()
        {
            var projects = await unitOFWork.ProjectRepository.GetAll();

            var result = projects
                .Join(await unitOFWork.TaskRepository.GetAll(), p => p.Id, t => t.ProjectId, (p, t) => new { p, t })
                .Join(await unitOFWork.UserRepository.GetAll(), e => e.p.TeamId, u => u.TeamId, (e, u) => new { Project = e.p, Task = e.t, User = u })
                .GroupBy(e => e.Project)
                .Select(e => new ProjectInfo
                {
                    Project = e.Key,
                    LongestTask = e.Select(x => x.Task).Where(x => x.Description.Length == e.Select(x => x.Task.Description.Length).Max()).FirstOrDefault(),
                    ShortestTask = e.Select(x => x.Task).Where(x => x.Name.Length == e.Select(x => x.Task.Name.Length).Min()).FirstOrDefault(),
                });

            return result;
        }
    }
}
