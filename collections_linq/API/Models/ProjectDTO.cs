﻿using System;
using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    public class ProjectDTO
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime Deadline { get; set; }
        [Required]
        public int AuthorId { get; set; }
        [Required]
        public int TeamId { get; set; }
    }
}
