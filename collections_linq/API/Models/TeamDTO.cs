﻿using System;
using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    public class TeamDTO
    {
        [Required]
        public string Name { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }
    }
}
