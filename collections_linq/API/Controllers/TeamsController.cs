﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using AutoMapper;
using BL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService teamService;
        private readonly IMapper mapper;

        public TeamsController(ITeamService teamService, IMapper mapper)
        {
            this.teamService = teamService;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> GetTeams()
        {
            return Ok(mapper.Map<IEnumerable<TeamDTO>>(await teamService.GetAll()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> GetTeamById(int id)
        {
            if (id < 1)
                return BadRequest();

            var team = await teamService.GetById(id);

            if (team == null)
                return NotFound();
            else
                return Ok(mapper.Map<TeamDTO>(team));
        }

        [HttpGet("MinAge/{age}")]
        public async Task<ActionResult<IEnumerable<object>>> GetTeamsOfCertainAge(int age)
        {
            if (age < 1)
                return BadRequest();

            var teams = await teamService.GetTeamsOfCertainAge(age);
            var result = teams.Select(e => new { e.Id, e.Name, Users = mapper.Map<IEnumerable<UserDTO>>(e.Users) });

            return Ok(result);
        }

        [HttpPost("multiple")]
        public async Task<IActionResult> AddTeams([FromBody] IEnumerable<TeamDTO> newTeams)
        {
            if (newTeams.Any(e => !TryValidateModel(e)))
                return BadRequest();

            await teamService.Add(mapper.Map<IEnumerable<Team>>(newTeams));

            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> AddTeam([FromBody] TeamDTO newTeam)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            await teamService.Add(mapper.Map<Team>(newTeam));

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove(int id)
        {
            if (id < 1)
                return BadRequest();

            var item = await teamService.GetById(id);

            if (item == null)
                return NotFound();

            try
            {
                await teamService.Remove(item);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }

        [HttpPatch("multiple")]
        public async Task<IActionResult> UpdateTeams([FromBody] IEnumerable<TeamDTO> newTeams)
        {
            try
            {
                await teamService.Update(mapper.Map<IEnumerable<Team>>(newTeams));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }

        [HttpPatch]
        public async Task<IActionResult> UpdateTeam([FromBody] TeamDTO newTeam)
        {
            try
            {
                await teamService.Update(mapper.Map<Team>(newTeam));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }
    }
}
