﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using AutoMapper;
using BL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService taskService;
        private readonly IMapper mapper;

        public TasksController(ITaskService taskService, IMapper mapper)
        {
            this.taskService = taskService;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TasksDTO>>> GetTasks()
        {
            return Ok(mapper.Map<IEnumerable<TasksDTO>>(await taskService.GetAll()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TasksDTO>> GetTaskById(int id)
        {
            if (id < 1)
                return BadRequest();

            var task = await taskService.GetById(id);

            if (task == null)
                return NotFound();
            else
                return Ok(mapper.Map<TasksDTO>(task));
        }

        [HttpGet("{id}/ByUser")]
        public async Task<ActionResult<Dictionary<string, int>>> GetNumberOfTasksByUser(int id)
        {
            if (id < 1)
                return BadRequest();

            var task = await taskService.GetById(id);

            if (task == null)
                return NotFound();

            return Ok(await taskService.GetNumberOfTasksByUser(id));
        }

        [HttpGet("{id}/WithMaxLength/{maxLength}")]
        public async Task<ActionResult<IEnumerable<TasksDTO>>> GetTasksByUserWithMaxNameLength(int id, int maxLength)
        {
            if (id < 1 || maxLength < 0)
                return BadRequest();

            var task = await taskService.GetById(id);

            if (task == null)
                return NotFound();

            return Ok(mapper.Map<TasksDTO>(await taskService.GetTasksByUserWithMaxNameLength(id, maxLength)));
        }

        [HttpGet("{id}/CurrentYear")]
        public async Task<ActionResult<IEnumerable<TasksDTO>>> GetTasksByUserInCurrentYear(int id)
        {
            if (id < 1)
                return BadRequest();

            var task = await taskService.GetById(id);

            if (task == null)
                return NotFound();

            return Ok(mapper.Map<TasksDTO>(await taskService.GetTasksByUserInCurrentYear(id)));
        }

        [HttpPost("multiple")]
        public async Task<IActionResult> AddTasks([FromBody] IEnumerable<TasksDTO> newTasks)
        {
            if (newTasks.Any(e => !TryValidateModel(e)))
                return BadRequest();

            await taskService.Add(mapper.Map<IEnumerable<Tasks>>(newTasks));

            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> AddTask([FromBody] TasksDTO newTask)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            await taskService.Add(mapper.Map<Tasks>(newTask));

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> RemoveTask(int id)
        {
            if (id < 1)
                return BadRequest();

            var item = await taskService.GetById(id);

            if (item == null)
                return NotFound();

            try
            {
                await taskService.Remove(item);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }

        [HttpPatch("multiple")]
        public async Task<IActionResult> UpdateTasks([FromBody] IEnumerable<TasksDTO> newTasks)
        {
            try
            {
                await taskService.Update(mapper.Map<IEnumerable<Tasks>>(newTasks));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }

        [HttpPatch]
        public async Task<IActionResult> UpdateTask([FromBody] TasksDTO newTask)
        {
            try
            {
                await taskService.Update(mapper.Map<Tasks>(newTask));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }
    }
}
